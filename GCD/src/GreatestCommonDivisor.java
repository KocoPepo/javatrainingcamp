
import java.util.InputMismatchException;
import java.util.Scanner;

public class GreatestCommonDivisor {

    public static void main(String[] args) {
        int num1 = getNumber();
        int num2 = getNumber();

        int min = num1 < num2 ? num1 : num2;
        min = Math.abs(min);
        while(!((num1 % min == 0) && (num2 % min == 0)))
            min--;
        System.out.println("Greatest common divisor is: " + min);
        System.out.println("Least common multiple is: " + (num1*num2)/min);

    }

    private static int getNumber() {
        int num = 0;
        try{
            System.out.println("Enter any natural number:");
            num = new Scanner(System.in).nextInt();
        }
        catch (InputMismatchException ex){
            System.out.println("Not a number!");
            num = getNumber();
        }
        return num;
    }
}
