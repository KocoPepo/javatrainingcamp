
import java.util.InputMismatchException;
import java.util.Scanner;

public class LeastCommonMultiple {
    static int num1 = 0;
    static int num2 = 0;
    static int max,mltp;

    public static void main(String[] args) {

        if(args.length != 2) {
            getNumbers();
        }
        else {
            try {
                num1 = Integer.parseInt(args[0]);
                num2 = Integer.parseInt(args[1]);
            }
            catch (NumberFormatException ex) {
                System.out.println("Arguments are not numbers");
                getNumbers();
            }
        }

        max = num1 > num2 ? num1 : num2;
        max = Math.abs(max);
        mltp = max;
        while(!((mltp % num1 == 0) && (mltp % num2 == 0))) {
            mltp = mltp + max;
        }
        System.out.println("Least Multiple is:" + mltp);
    }

    protected static void getNumbers() {
        Scanner sc = new Scanner(System.in);
        try{
            System.out.println("Enter first number:");
            num1 = sc.nextInt();
            System.out.println("\nEnter second number:");
            num2 = sc.nextInt();
        }
        catch (InputMismatchException ex){
            System.out.println("Not a number!");
            getNumbers();
        }
    }
}
