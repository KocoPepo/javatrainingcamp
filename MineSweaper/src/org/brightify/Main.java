package org.brightify;

import org.brightify.minesweaper.Board;

public class Main {

    public static void main(String[] args) {
        Board b = Board.createBoard(5,10);
        b.newGame(10);
        b.printBoard();
    }
}
