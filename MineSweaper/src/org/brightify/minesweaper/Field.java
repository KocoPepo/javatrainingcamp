package org.brightify.minesweaper;

/**
 * Políčko na hracej ploche
 *
 * @author Martin Kocour
 *         created on 2.4.2015.
 */
public class Field {
    /* suradnice polohy na hracej ploche */
    private int row;
    private int col;
    private Card card;

    public Field (int r, int c) {
        row = r;
        col = c;
        card = null;
    }

    public int row() {
        return row;
    }

    public int col() {
        return col;
    }

    public Card getCard() {
        return card;
    }

    public void putCard( Card c ) {
        this.card = c;
    }

}
