package org.brightify.minesweaper;

/**
 * Hracia karta na policku
 *
 * reprezentuje bud mínu alebo číslo alebo hole policko
 *
 * @author Martin Kocour
 *         created on 2.4.2015.
 */
public class Card {
    private boolean revealed;
    private CARD_TYPE type;
    private int number;

    private Card (CARD_TYPE type) {
        this.type = type;
        revealed = false;
        number = 0;
    }

    public static enum CARD_TYPE {
        MINE, PLAIN, NUMBER;
    }

    public static Card create(String type) throws IllegalArgumentException {
        switch (type) {
            case "MINE":
                return new Card(CARD_TYPE.MINE);
            case "PLAIN":
                return new Card(CARD_TYPE.PLAIN);
            case "NUMBER":
                return new Card(CARD_TYPE.NUMBER);
            default:
                throw new IllegalArgumentException("Unknown type");
        }
    }

    public boolean isRevealed() {
        return revealed;
    }

    public CARD_TYPE getType () {
        return type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int n) {
        this.number = n;
    }
}
