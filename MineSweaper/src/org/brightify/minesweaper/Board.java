package org.brightify.minesweaper;

import java.util.Random;

/**
 * Hracia plocha
 *
 * @author Martin Kocour
 *         created on 2.4.2015.
 */
public class Board {
    private Field fields[][];
    private int mineCount;
    private int width;
    private int height;

    private Board(int h, int w) {
        width = w;
        height = h;
        mineCount = 0;
        fields = new Field[h][w];
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                fields[i][j] = new Field(i, j);
                Card c = Card.create("PLAIN");
                fields[i][j].putCard(c);
            }
        }
    }

    public static Board createBoard (int h, int w) {
        if(h > 0 && w > 0)
            return new Board(h, w);
        else
            return null;
    }

    public void newGame(int mines) {
        if(mines < (width * height)) {
            /* set mine fields */
            loadMines(mines);
            /* compute numbers */
            setNumbers();
            /* play */
        }
    }

    private void loadMines(int mines) {
        this.mineCount = mines;
        for(int i = 1; i <= mines; i++) {
            int row;
            int col;
            do {
                row = getRndNumber() % height;
                col = getRndNumber() % width;
            } while (fields[row][col].getCard().getType().equals(Card.CARD_TYPE.MINE));
            Card c = Card.create("MINE");
            fields[row][col].putCard(c);
        }
    }

    private void setNumbers() {
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                if(!fields[row][col].getCard().getType().equals(Card.CARD_TYPE.MINE)) {
                    int number = getMinesInNeighborhood(row, col);
                    if(number > 0) {
                        Card c = Card.create("NUMBER");
                        c.setNumber(number);
                        fields[row][col].putCard(c);
                    }
                }
            }
        }
    }

    private int getMinesInNeighborhood(int row, int col) {
        int number = 0;

        Field fld = getField(row+1, col);
        if(fld!= null && fld.getCard().getType().equals(Card.CARD_TYPE.MINE))
            number++;

        fld = getField(row-1, col);
        if(fld!= null && fld.getCard().getType().equals(Card.CARD_TYPE.MINE))
            number++;

        fld = getField(row, col+1);
        if(fld!= null && fld.getCard().getType().equals(Card.CARD_TYPE.MINE))
            number++;

        fld = getField(row, col-1);
        if(fld!= null && fld.getCard().getType().equals(Card.CARD_TYPE.MINE))
            number++;

        fld = getField(row+1, col+1);
        if(fld!= null && fld.getCard().getType().equals(Card.CARD_TYPE.MINE))
            number++;

        fld = getField(row+1, col-1);
        if(fld!= null && fld.getCard().getType().equals(Card.CARD_TYPE.MINE))
            number++;

        fld = getField(row-1, col+1);
        if(fld!= null && fld.getCard().getType().equals(Card.CARD_TYPE.MINE))
            number++;

        fld = getField(row-1, col-1);
        if(fld!= null && fld.getCard().getType().equals(Card.CARD_TYPE.MINE))
            number++;

        return number;
    }

    private static int getRndNumber() {
        Random rnd = new Random();
        int number = rnd.nextInt(Integer.MAX_VALUE);
        number = Math.abs(number);
        return number;
    }

    private Field getField(int r, int c) {
        if(r >= 0 && r < height && c >= 0 && c < width)
            return fields[r][c];
        else
            return null;
    }

    public void printBoard() {
        System.out.println("Board "+height+" * "+width);
        for (int row =0; row < height; row++){
            System.out.print("|");
            for (int col = 0; col < width; col++){
                if(fields[row][col].getCard().getType().equals(Card.CARD_TYPE.MINE))
                    System.out.print("*|");
                else if(fields[row][col].getCard().getType().equals(Card.CARD_TYPE.NUMBER))
                    System.out.print(fields[row][col].getCard().getNumber()+"|");
                else
                    System.out.print(" |");
            }
            System.out.println("");
        }
    }
}
