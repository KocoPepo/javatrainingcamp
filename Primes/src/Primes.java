import java.util.InputMismatchException;
import java.util.Scanner;

public class Primes {

    public static void main(String[] args) {
        int counter = getNumber();

        System.out.println("First "+ counter + " primes are:" );
        int[] primes = new int[counter];
        primes[0] = 2;
        int number = 3;
        for(int i = 1; i < counter; number++){
            boolean isPrime = true;
            for(int j = 0; j < primes.length && primes[j] != 0; j++){
                if(primes[j] > Math.sqrt(number)){
                    break;
                }
                if(number % primes[j] == 0) {
                    isPrime = false;
                    break;
                }
            }
            if(isPrime) {
                primes[i] = number;
                System.out.println(number);
                i++;
            }
        }
    }

    private static int getNumber() {
        int num;
        try {
            System.out.println("Enter limit:");
            num = new Scanner(System.in).nextInt();
        }
        catch (InputMismatchException ex) {
            System.out.println("Wrong format of number enter limit again!");
            num = getNumber();
        }
        return num;
    }
}
